use std::{error::Error, fs::File, path::Path, process::exit};

use csv::Reader;
use flashcard::Flashcard;

mod flashcard;

fn main() {
    run_path(Path::new("./germany_dates.csv"));

}

fn run_path(path: &Path) {
    let mut flashcards: Vec<Flashcard> = vec![];
    let mut summing_flashcards: Vec<Flashcard> = vec![];
    let mut rdr = match read_from_file(path) {
        Ok(rdr) => rdr,
        Err(e) => {
            println!("{}", e);
            exit(1)
        },
    };

    let mut current_year = String::new();
    let mut flashcards_this_year = vec![];

    for record in rdr.records() {
        let record = record.unwrap();
        let mut record: Vec<&str> = record.iter().collect();
        record.retain(|x| !x.trim().is_empty());
        if record.len() == 1 {
            if !flashcards_this_year.is_empty() {
                summing_flashcards.insert(summing_flashcards.len(), assemble_summing_card(current_year, &flashcards_this_year));
                flashcards_this_year = vec![];
            }
            current_year = record[0].to_owned();
        } else if record.len() == 2 {
            let f = Flashcard {
                question: record[0].to_owned(),
                answer: record[1].to_owned(),
                question_type: flashcard::QuestionType::Memory,
            };

            flashcards.push(f.clone());
            flashcards_this_year.push(f)
        }
    }
    if !flashcards_this_year.is_empty() {
        summing_flashcards.insert(summing_flashcards.len(), assemble_summing_card(current_year, &flashcards_this_year))
    }

    let json_path = path.with_extension("json");

    let f = File::create(json_path).unwrap();
    flashcards.extend(summing_flashcards);
    let _ = serde_json::to_writer(f, &flashcards);
}

fn read_from_file(filepath: &Path) -> Result<Reader<File>, Box<dyn Error>> {
    let rdr = Reader::from_path(filepath)?;
    Ok(rdr)
}

fn assemble_summing_card(year: String, flashcards: &[Flashcard]) -> Flashcard {
    let mut answer = String::new();
    let answers = flashcards.len().to_string();

    for q in flashcards.iter().cloned() {
        answer += &("- ".to_owned() + q.question.trim() + " _(" + q.answer.trim() + ")_\n")
    }

    answer = answer.trim_end().to_string();

    Flashcard {
        question: "List the events in ".to_owned() + &year + ". _(" + &answers + ")_",
        answer,
        question_type: flashcard::QuestionType::Memory,
    }
}