use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone, Copy)]
pub enum QuestionType {
    Memory,
}

impl From<QuestionType> for String {
    fn from(val: QuestionType) -> Self {
        match val {
            QuestionType::Memory => "Memory".to_owned(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Flashcard {
    pub question: String,
    pub answer: String,
    pub question_type: QuestionType
}





